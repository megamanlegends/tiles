
"use strict";


const TileEditor = (function() {

	this.MEM = {
		scene : new THREE.Scene(),
		camera : new THREE.PerspectiveCamera(),
		clock : new THREE.Clock(),
		renderer : new THREE.WebGLRenderer(),
		raycaster : new THREE.Raycaster(),
		mouse : { x : 0, y : 0 }
	};

	this.DOM = {
		viewport : document.getElementById('TileEditor.viewport'),
		file : document.getElementById('TileEditor.file')
	};

	this.EVT = {
		handleFileChange : evt_handleFileChange.bind(this),
		handleWindowResize : evt_handleWindowResize.bind(this),
		handleMouseMove : evt_handleMouseMove.bind(this)
	};

	this.API = {
		setViewport : api_setViewport.bind(this),
		renderViewport : api_renderViewport.bind(this),
		setFloorTiles : api_setFloorTiles.bind(this),
		setAvailableTiles : api_setAvailableTiles.bind(this)
	};

	init.apply(this);
	return this;

	function init() {

		// Initialize Three.js

		this.MEM.camera.position.z = 55;
		this.MEM.camera.position.y = 45;
		this.MEM.camera.position.x = 10;
		this.MEM.camera.lookAt(new THREE.Vector3(0, 25, 0));

		this.MEM.renderer.domElement.style.margin = "0";
		this.MEM.renderer.domElement.style.padding = "0";
		this.MEM.renderer.setClearColor(0xeeeeee, 1);

		this.DOM.viewport.appendChild(this.MEM.renderer.domElement);
		THREE.OrbitControls(this.MEM.camera, this.DOM.viewport);
		
		this.API.setViewport();
		this.API.renderViewport();
		this.API.setFloorTiles(20, 20);

		// Event Listeners
		
		this.DOM.file.addEventListener('change', this.EVT.handleFileChange);
		this.DOM.viewport.addEventListener('mousemove', this.EVT.handleMouseMove);

		// Set scene

		this.MEM.scene.add(new THREE.AmbientLight( 0xffffff ));

	}

	function evt_handleFileChange(evt) {

		if(!evt.target.files) {
			return;
		}

		if(!evt.target.files.length) {
			return;
		}

		let file = evt.target.files[0];
		
		let reader = new FileReader();

		reader.onload = res => {
			
			let loader = new THREE.GLTFLoader();
			loader.parse(res.target.result, null, gltf => {
				
				this.API.setAvailableTiles(gltf.scene.children);

			});

		}

		reader.readAsArrayBuffer(file);

		/*
		let reader = new FileReader();

		reader.onload = res => {
			
			let loader = new THREE.ColladaLoader();
			let dae = loader.parse(res.target.result, "");
			this.API.setAvailableTiles(dae.scene.children);

		}

		reader.readAsText(file);
		*/

	}

	function evt_handleWindowResize() {
		
		this.API.setViewport();

	}

	function evt_handleMouseMove(evt) {
		
		this.MEM.mouse.x = ( (event.clientX - 240)/ this.MEM.renderer.domElement.clientWidth ) * 2 - 1;
		this.MEM.mouse.y = - ( (event.clientY- 40) / this.MEM.renderer.domElement.clientHeight ) * 2 + 1;
		this.MEM.raycaster.setFromCamera( this.MEM.mouse, this.MEM.camera );

		let intersects = this.MEM.raycaster.intersectObjects( this.MEM.tiles );
		
		if(!intersects.length) {

			if(this.MEM.activeTile) {
				this.MEM.activeTile.material.color.setHex( 0xffffff );
			}

			return;
		}

		let tile = intersects[0].object;
		if(this.MEM.activeTile === tile) {
			return;
		}

		if(this.MEM.activeTile) {
			this.MEM.activeTile.material.color.setHex( 0xffffff );
		}
		
		this.MEM.activeTile = tile;
		this.MEM.activeTile.material.color.setHex( 0xff0000 );

	}

	function api_setViewport() {

		let width = this.DOM.viewport.offsetWidth;
		let height = this.DOM.viewport.offsetHeight;

		this.MEM.camera.aspect = width / height;
		this.MEM.camera.updateProjectionMatrix();
		this.MEM.renderer.setSize( width - 2, height - 2);

	}

	function api_renderViewport() {

		requestAnimationFrame(this.API.renderViewport);
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);

	}

	function api_setFloorTiles(xDis, yDis) {

		const LEN = 3.200000047683716;
		const geometry = new THREE.PlaneBufferGeometry(LEN, LEN);

		const line = new THREE.MeshBasicMaterial({
			color: 0x0, 
			side: THREE.BackSide, 
			wireframe: true
		});

		const wire = new THREE.LineSegments( geometry, line);
		
		this.MEM.tiles = [];
		let xHalf = (xDis * LEN) / 2;
		let yHalf = (yDis * LEN) / 2;

		for(let y = 0; y < yDis; y++) {
			
			for(let x = 0; x < xDis; x++) {

				const material = new THREE.MeshBasicMaterial({
					polygonOffset: true,
				    polygonOffsetFactor: 1,
				    polygonOffsetUnits: 1,
					side : THREE.DoubleSide,
					transparent: true,
					opacity: 0.2
				});

				let square = new THREE.Mesh( geometry, material );
				square.rotation.x = -Math.PI / 2;

				square.position.x = x * LEN - xHalf;
				square.position.z = y * LEN - yHalf;
				var box = new THREE.BoxHelper( square, 0x000000 );
				this.MEM.scene.add(square);
				this.MEM.scene.add(box);
				this.MEM.tiles.push(square);

			}

		}

	}

	function api_setAvailableTiles(tiles) {

		if(tiles.length === 0) {
			return;
		}

		console.log(tiles[0]);
		
		let flat = tiles[0];
		flat.position.x = 0;
		flat.position.y = 0;
		flat.position.z = 0;
		
		var bbox = new THREE.Box3().setFromObject(flat);
		this.MEM.scene.add(flat);

	}

}).apply({});
